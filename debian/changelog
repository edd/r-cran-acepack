acepack (1.6.1-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 16 Feb 2025 21:55:26 -0600

acepack (1.5.2-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 27 Jan 2025 11:21:58 -0600

acepack (1.5.1-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 22 Jan 2025 07:13:05 -0600

acepack (1.5.0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 21 Jan 2025 15:16:21 -0600

acepack (1.4.2-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Switch to virtual debhelper-compat (= 13)
  * debian/compat: Removed
  * debian/control: Add Vcs-Browser: and Vcs-Git:
  * debian/control: Switch from cdbs to dh-r
  * debian/rules: Idem

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 26 Aug 2023 08:32:08 -0500

acepack (1.4.1-2) unstable; urgency=medium

  * Rebuilt under R 3.4.0 to update registration for .C() and .Fortran()
  
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 03 Jun 2017 17:41:10 -0500

acepack (1.4.1-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 31 Oct 2016 20:21:01 -0500

acepack (1.4.0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 27 Oct 2016 06:35:26 -0500

acepack (1.3.3.3-2) unstable; urgency=medium

  * debian/compat: Change level to 7 			(Closes: #829109)
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 01 Jul 2016 19:01:03 -0500

acepack (1.3.3.3-1) unstable; urgency=low

  * New upstream release -- and reinsertion into Debian as the package,
    which had orphaned upstream, now has a new upstream maintainer
							    (Closes: #761506)

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
 
  * debian/copyright: Updated

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 14 Sep 2014 07:45:37 -0500

acepack (1.3.2.2-3) unstable; urgency=low

  * Rebuilt for R 2.10.0 to work with new R-internal help file conversion

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  
  * debian/control: Changed Section: to section 'gnu-r'

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 01 Nov 2009 08:14:53 -0600

acepack (1.3.2.2-2) unstable; urgency=low

  * debian/rules: Simplified to cdbs-based one-liner sourcing r-cran.mk 
  * debian/control: Updated (Build-)Depends: to r-base-dev (>= 2.6.0)
  * debian/control: Standards-Version: increased to 3.7.2

  * debian/watch: Corrected regular expression		(Closes: #450263)

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 06 Nov 2007 20:41:36 -0600

acepack (1.3.2.2-1) unstable; urgency=low

  * New upstream release
  * debian/watch: Corrected regular expression (thanks, Rafael Laboissier)
  * debian/post{inst,rm}: Call /usr/bin/R explicitly (thanks, Kurt Hornik)
  * debian/control: Upgraded Standards-Version: to 3.6.2.1

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 13 Aug 2005 09:23:47 -0500

acepack (1.3.2.1-2) unstable; urgency=low

  * Rebuilt under R 2.0.0
  * debian/control: Updated Build-Depends: and Depends: accordingly
  * debian/control: Added r-cran-hmisc, r-cran-lattice, r-cran-acepack 

 -- Dirk Eddelbuettel <edd@debian.org>  Fri,  8 Oct 2004 19:20:04 -0500

acepack (1.3.2.1-1) unstable; urgency=low

  * Initial Debian Release

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 11 Sep 2004 21:47:31 -0500


